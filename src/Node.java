import java.util.*;

//https://docs.oracle.com/javase/7/docs/api/javax/swing/tree/TreeNode.html
//http://stackoverflow.com/questions/10290649/tree-of-nodet-explaination
//https://bitbucket.org/akopolov/algorithms/

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.nextSibling = r;
      this.firstChild = d;
   }
   
   public static Node parsePostfix (String s) {
      int i = 0;
      if (s.trim().isEmpty()) {
         throw new RuntimeException("String is empty");
      }
       String sCopy= s.replaceAll(", " , ",");
      sCopy = sCopy.replaceAll(" ,", ",");
      LinkedList<String> copyItems = new LinkedList<>(Arrays.asList(sCopy.split("")));
      for (String item: copyItems) {
         if(item.replaceAll("\\s+", " ").equals(" ")) {
            throw new RuntimeException("Whitespace in string (except trailing space after comma): \"" + s + "\"");
         }
         if (item.equals("(")) {
            i++;
         } else if (item.equals(")")) {
            i--;
         } else if (item.equals(",") && i == 0) {
            throw new RuntimeException("Input string missing brackets: \"" + s + "\"");
         }
      }

      if (i < 0) {
         throw new RuntimeException("Too many left brackets: \"" + s + "\"");
      } else if (i > 1) {
         throw new RuntimeException("Too many right brackets: \"" + s + "\"");
      }

      LinkedList<String> items = new LinkedList<>(Arrays.asList(s.split("")));
      Node root = createTree(new Node(null, null, null), items, s);

      if(root.firstChild == null && root.nextSibling == null) {
         if (root.name != null) {
            return root;
         }
      } else if (root.firstChild == null) {
         throw new RuntimeException("Missing brackets in string: \"" + s + "\"");
      }
      return root;
   }

   public String leftParentheticRepresentation() {
      return stringBuild(this).name;
   }

   public static void main (String[] param) {
      String s = "%%";
//      String s = "((@,#)+)-34";
//      String s = "((@, #)+)-34";
//      String s = "     \t ";
//      String s = "((1),(2)3)4";
//      String s = "A B";
//      String s = "((3 , 5 )6)7";

      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println ("Input: " + s);
      System.out.println ("Left: " + v);
   }


   //HELPERS

   public static Node createTree(Node root, LinkedList<String> items, String string) {
      while (!items.isEmpty()) {
         String item = items.pop();
         switch (item) {
            case "(":
               root.firstChild = createTree(new Node(null, null, null), items, string);
               break;
            case ",":
               root.nextSibling = createTree(new Node(null, null, null), items, string);
               if (root.name == null) {
                  throw new RuntimeException("Missing Node name OR ,, in string: \"" + string + "\"");
               }
               return root;
            case ")":
               if (root.name == null) {
                  throw new RuntimeException("empty(X) OR X(empty) OR (empty)X OR (X)empty OR empty(empty) in string: \"" + string + "\"");
               }
               return root;
            default:
               if (root.name == null) {
                  root.name = item;
               } else {
                  root.name += item;
               }
               break;
         }
      }
      if (root.name == null) {
         throw new RuntimeException("empty, OR ,empty in string: \"" + string + "\"");
      }
      return root;
   }

   public static Node stringBuild(Node root) {
      if (root.firstChild != null) {
         root.name += "(" + stringBuild(root.firstChild).name;
         root.name += ")";
         if (root.nextSibling != null) {
            root.name += "," + stringBuild(root.nextSibling).name;
            return root;
         }
      } else if (root.nextSibling != null) {
         root.name += "," + stringBuild(root.nextSibling).name;
         return  root;
      } else {
         return root;
      }
      return root;
   }
}